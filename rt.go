/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// rt.go: Exported types and functions common to all realtime implementations

package rt

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"math"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"
)

var taskMapping bool
var taskMappingMtx sync.Mutex
var taskMap TaskTimingInformation

// A Task represents the configuration for an os thread.
type Task struct {
	Name     string // Name of thread when supported.
	CPU      []int  // CPU affinity, empty to allow all CPUs
	Period   time.Duration
	Runtime  time.Duration
	Deadline time.Duration
}

// A Thread represents an existing os thread as created by this package's LockOSThread.
type Thread interface {
	// Yield should be called when you're done requiring CPU time this period.  Otherwise, if the
	// os thread blocks and resumes before its deadline hits, it will continue.  If it tries to
	// resume after its deadline, it will have a low priority.  The best way to ensure the os
	// thread resumes correctly is to call Yield.
	Yield() error

	// Returns the name passed to LockOSThread
	Name() string

	EnableStats(bool) bool

	Stats() *ThreadStats
}

// DurationStat holds various useful stats about a duration measurement
type DurationStat struct {
	Last    time.Duration
	Min     time.Duration
	Max     time.Duration
	Average time.Duration
	Total   time.Duration // Used for calculating Average
	Count   int           // Used for calculating Average
}

func (ds *DurationStat) add(d time.Duration) {
	ds.Last = d
	if d < ds.Min {
		ds.Min = d
	}
	if d > ds.Max {
		ds.Max = d
	}
	ds.Total += d
	ds.Count++
	ds.Average = ds.Total / time.Duration(ds.Count)
}

// ThreadStats contains statistics about a Thread.
type ThreadStats struct {
	LastYieldTime        time.Time
	LastYieldWakeTime    time.Time
	NextExpectedWakeTime time.Time `;not handled`
	RunTime              DurationStat
	YieldTime            DurationStat
	MeasuredPeriod       DurationStat
	YieldCount           int
	DeadlineMissCount    int  `;not handled`
	MissedLastDeadline   bool `;not handled`
}

// LockOSThread calls runtime.LockOSThread, then calls OS specific functions to set the thread name
// and setup realtime scheduling.  This will use SCHED_DEADLINE on linux.  Goroutines that call
// this can launch other goroutines, but the new ones will not have the same prorities and will not
// be locked to an os thread.
func LockOSThread(t Task) (Thread, error) {
	// debug.SetGCPercent(-1)
	runtime.LockOSThread()

	th, err := lockOSThread(t)
	if err != nil {
		return nil, err
	}

	if taskMapping {
		taskMappingMtx.Lock()
		if taskMap == nil {
			taskMap = make(map[TaskTimingInformationKey]TaskTimingInformationValue)
		}
		k := TaskTimingInformationKey{Period: t.Period}
		if _, ok := taskMap[k]; !ok {
			taskMap[k] = nil
		}
		taskMap[k] = append(taskMap[k], TaskTimingElem{
			Name:     t.Name,
			Runtime:  t.Runtime,
			Deadline: t.Deadline,
			Affinity: t.CPU,
		})
		sort.Slice(taskMap[k], func(i, j int) bool {
			return taskMap[k][i].Deadline < taskMap[k][j].Deadline
		})
		var usedRuntime time.Duration
		for i, v := range taskMap[k] {
			taskMap[k][i].PercentPeriod = float64(v.Runtime-usedRuntime) / float64(k.Period) * 100
		}
		taskMappingMtx.Unlock()
	}

	return th, nil
}

//go:noescape
func osyield()

// EnableTaskMap sets up tracking of calls to LockOSThread for the purpose of understanding the
// timing between the Threads with the TaskTiming function.  Call this before calling LockOSThread
// or TaskTiming will be missing information.
func EnableTaskMap() {
	taskMapping = true
}

// TaskTiming will return timing information for the Threads created after calling EnableTaskMap.
func TaskTiming() TaskTimingInformation {
	m := make(TaskTimingInformation)
	var b bytes.Buffer

	taskMappingMtx.Lock()

	e := gob.NewEncoder(&b)
	e.Encode(taskMap)

	taskMappingMtx.Unlock()

	d := gob.NewDecoder(&b)
	d.Decode(&m)

	return m
}

// TaskTimingInformation contains a list of all registered tasks and their properties
type TaskTimingInformation map[TaskTimingInformationKey]TaskTimingInformationValue

// TaskTimingInformationKey is the key for the TaskTimingInformation map containing the properties
// that, combined, make sense to maintain a seperate TaskTimingInformationValue.
type TaskTimingInformationKey struct {
	Period time.Duration
}

// TaskTimingInformationValue contains the timing information for Threads created after calling
// EnableTaskMap.
type TaskTimingInformationValue []TaskTimingElem

// TaskTimingElem contains the timing information for a single Thread.
type TaskTimingElem struct {
	Name          string
	Runtime       time.Duration
	Deadline      time.Duration
	PercentPeriod float64
	Affinity      []int
}

const singleGraphWidth = 40

// EncodeToString will create an ASCII art plot of a TaskTimingInformation
func (t TaskTimingInformation) EncodeToString() string {
	om := t

	// sorted map keys for consistent output
	sk := make([]TaskTimingInformationKey, len(om))
	i := 0
	for k := range om {
		sk[i] = k
		i++
	}
	sort.Slice(sk, func(i, j int) bool {
		return sk[i].Period < sk[j].Period
	})

	// sort by earliest deadline
	for _, k := range sk {
		sort.Slice(om[k], func(i, j int) bool {
			return om[k][i].Deadline < om[k][j].Deadline
		})
	}

	s := "Task Timing Info: \n"
	for _, k := range sk {
		s += "\t" + fmt.Sprint(k) + " Period ("
		for i, v := range om[k] {
			s += fmt.Sprintf("%s: '%c', ", v.Name, 'a'+i)
		}
		s += "unallocated: ',')\n"
	}

	s += "\t\t+" + string(bytes.Repeat([]byte{'-'}, singleGraphWidth)) + "+\n"
	s += "\t\t| Full Deadline" + string(bytes.Repeat([]byte{' '}, singleGraphWidth-14)) + "|\n"
	for _, k := range sk {
		strs := make([]string, len(om[k])+1)
		var usedDeadline time.Duration
		for i, v := range om[k] {
			strs[i] = string(bytes.Repeat([]byte{byte('a' + i)}, int(math.Max(1, float64(v.Deadline-usedDeadline)/float64(k.Period)*singleGraphWidth))))
			usedDeadline += v.Deadline - usedDeadline
		}
		strs[len(om[k])] = string(bytes.Repeat([]byte{byte(',')}, int(math.Max(0, float64(singleGraphWidth-len(strings.Join(strs, "")))))))
		s += "\t\t|" + fmt.Sprint(strings.Join(strs, "")[:singleGraphWidth]) + "| " + fmt.Sprint(k) + "Period , " + fmt.Sprint(k.Period-usedDeadline) + " Remains\n"
	}
	s += "\t\t+" + string(bytes.Repeat([]byte{'-'}, singleGraphWidth)) + "+\n"

	s += "\t\t| Full Runtime" + string(bytes.Repeat([]byte{' '}, singleGraphWidth-13)) + "|\n"
	for _, k := range sk {
		strs := make([]string, len(om[k])+1)
		var usedRuntime time.Duration
		for i, v := range om[k] {
			strs[i] = string(bytes.Repeat([]byte{byte('a' + i)}, int(math.Max(1, float64(v.Runtime-usedRuntime)/float64(k.Period)*singleGraphWidth))))
			usedRuntime += v.Runtime - usedRuntime
		}
		strs[len(om[k])] = string(bytes.Repeat([]byte{byte(',')}, int(math.Max(0, float64(singleGraphWidth-len(strings.Join(strs, "")))))))
		s += "\t\t|" + fmt.Sprint(strings.Join(strs, "")[:singleGraphWidth]) + "| " + fmt.Sprint(k) + "Period , " + fmt.Sprint(k.Period-usedRuntime) + " Remains\n"
	}
	s += "\t\t+" + string(bytes.Repeat([]byte{'-'}, singleGraphWidth)) + "+\n"

	var largestPeriod time.Duration
	if len(sk) > 0 {
		largestPeriod = sk[len(sk)-1].Period
	}
	for i := 0; i < len(sk)-1; i++ {
		if largestPeriod%sk[i].Period != 0 {
			s += "\tWarning: Periods don't sync\n"
			break
		}
	}

	return s
}

func init() {
	// debug.SetGCPercent(-1)
}
