/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// rt_linux.go: Linux-specific implementation for realtime apps using sched_deadline

package rt

import (
	"fmt"
	"os"
	"runtime"
	"sort"
	"strings"
	"sync/atomic"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

type linuxThread struct {
	task Task

	initialized bool
	tid         int

	statsEnabled bool
	stats        atomic.Value
}

var _ Thread = &linuxThread{}

const schedDeadline uint32 = 6

func lockOSThread(task Task) (Thread, error) {
	var thread linuxThread

	thread.task = task

	err := thread.setThreadName(task.Name)
	if err != nil {
		return nil, err
	}
	err = thread.setAffinity(task.CPU)
	if err != nil {
		return nil, err
	}
	err = thread.applySchedDeadline(task)
	if err != nil {
		return nil, err
	}

	thread.tid = unix.Gettid()
	thread.initialized = true
	ts := ThreadStats{}
	// ts.LastYieldWakeTime = time.Now()
	thread.stats.Store(&ts)

	return &thread, nil
}

const _NoArg uintptr = 0

func (thread *linuxThread) setAffinity(cpu []int) error {
	// switch to using: github.com/containerd/cgroups

	if len(cpu) == 0 {
		return nil
	}

	for _, v := range cpu {
		if v < 0 || v >= runtime.NumCPU() {
			return fmt.Errorf("Invalid cpu: %d", v)
		}
	}

	sort.Ints(cpu)

	cpustr := make([]string, len(cpu))
	for i, v := range cpu {
		cpustr[i] = fmt.Sprint(v)
	}
	newAffinity := strings.Join(cpustr, ",")

	err := setAffinity(newAffinity)
	if err != nil {
		return err
	}
	return nil
}

func setAffinity(cNew string) (err error) {
	cgroupLeaf := "go-rt_cpu" + cNew
	// cg, err := cgroups.New(cgroups.V1, cgroups.StaticPath("/"+cgroupLeaf), &specs.LinuxResources{
	// 	CPU: &specs.LinuxCPU{
	// 		Cpus: cNew,
	// 	},
	// })
	// if err != nil {
	// 	log.Println("cg.New failed")
	// 	return err
	// }
	// if err := cg.AddTask(cgroups.Process{Pid: unix.Gettid()}); err != nil {
	// 	log.Println("cg.AddTask failed")
	// 	return err
	// }

	// TODO: fix hard coded path by figuring out how to get it from package cgroups
	err = os.MkdirAll("/sys/fs/cgroup/cpuset/"+cgroupLeaf, 0755)
	if err != nil {
		return err
	}

	root := "/sys/fs/cgroup/cpuset/" + cgroupLeaf + "/"

	err = echostr(cNew, root+"cpuset.cpus")
	if err != nil {
		return err
	}
	err = echonum(0, root+"cpuset.mems")
	if err != nil {
		return err
	}
	err = echonum(1, root+"cpuset.cpu_exclusive")
	if err != nil {
		return err
	}
	err = echonum(1, root+"cpuset.mem_exclusive")
	if err != nil {
		return err
	}
	err = echonum(unix.Gettid(), root+"tasks")
	if err != nil {
		return err
	}

	return nil
}

func echonum(num int, path string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintf(f, "%d", num)
	if err != nil {
		return err
	}

	return nil
}
func echostr(str string, path string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintf(f, "%s", str)
	if err != nil {
		return err
	}

	return nil
}

func (thread *linuxThread) setThreadName(name string) error {
	_, err := setThreadName(name)
	if err != nil {
		return err
	}
	return nil
}

func setThreadName(newname string) (oldname string, err error) {
	oldCname := make([]byte, 16)
	err = unix.Prctl(unix.PR_GET_NAME, uintptr(unsafe.Pointer(&oldCname[0])), _NoArg, _NoArg, _NoArg)
	if err != nil {
		return "", err
	}
	oldname = strings.SplitN(string(oldCname), "\x00", 2)[0]

	newCname := append([]byte(newname), 0)
	if len(newCname) > 16 {
		return "", fmt.Errorf("Thread name (length: %d) exceeds limit of 16 bytes including terminating null", len(newCname))
	}
	return oldname, unix.Prctl(unix.PR_SET_NAME, uintptr(unsafe.Pointer(&newCname[0])), _NoArg, _NoArg, _NoArg)
}

type schedAttr struct {
	size     uint32
	policy   uint32
	flags    uint64
	nice     uint32
	priority uint32
	runtime  uint64
	deadline uint64
	period   uint64
}

func (thread *linuxThread) applySchedDeadline(t Task) error {
	var err error
	_, err = schedSetAttr(schedAttr{
		policy:   schedDeadline,
		period:   uint64(t.Period.Nanoseconds()),
		deadline: uint64(t.Deadline.Nanoseconds()),
		runtime:  uint64(t.Runtime.Nanoseconds()),
	})
	return err
}

func schedSetAttr(sa schedAttr) (oldAttr schedAttr, err error) {
	oldAttr.size = uint32(unsafe.Sizeof(oldAttr))
	_, _, errno := unix.Syscall6(unix.SYS_SCHED_GETATTR, 0, uintptr(unsafe.Pointer(&oldAttr)), uintptr(oldAttr.size), 0, _NoArg, _NoArg)
	if errno != 0 {
		return oldAttr, fmt.Errorf("sched_getattr, kernel returned size: %d, err: %w", oldAttr.size, errno)
	}

	sa.size = uint32(unsafe.Sizeof(sa))
	_, _, errno = unix.Syscall6(unix.SYS_SCHED_SETATTR, 0, uintptr(unsafe.Pointer(&sa)), 0, _NoArg, _NoArg, _NoArg)
	if errno != 0 {
		return oldAttr, fmt.Errorf("sched_setattr, kernel returned size: %d, err: %w", sa.size, errno)
	}

	return oldAttr, nil
}

func (thread *linuxThread) Yield() error {
	var err error = nil
	if thread.statsEnabled {
		ts := thread.stats.Load().(*ThreadStats)
		t := time.Now()
		ts.LastYieldTime = t
		if ts.LastYieldWakeTime != (time.Time{}) {
			ts.RunTime.add(t.Sub(ts.LastYieldWakeTime))
		}
		ts.YieldCount++
		thread.stats.Store(ts)
	}
	// log.Println("before yield")
	_, _, errno := unix.Syscall(unix.SYS_SCHED_YIELD, _NoArg, _NoArg, _NoArg)
	// osyield()
	// log.Println("after yield")
	if thread.statsEnabled {
		t := time.Now()
		ts := thread.stats.Load().(*ThreadStats)
		ts.YieldTime.add(t.Sub(ts.LastYieldTime))
		if ts.LastYieldWakeTime != (time.Time{}) {
			ts.MeasuredPeriod.add(t.Sub(ts.LastYieldWakeTime))
		}
		ts.LastYieldWakeTime = t
		thread.stats.Store(ts)
	}
	if errno != 0 {
		err = errno
	}
	return err
}

func (thread *linuxThread) EnableStats(s bool) bool {
	en := thread.statsEnabled
	thread.statsEnabled = s
	return en
}

func (thread *linuxThread) Stats() *ThreadStats {
	return thread.stats.Load().(*ThreadStats)
}

func (thread *linuxThread) Name() string {
	return thread.task.Name
}
