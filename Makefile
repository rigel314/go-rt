none:
	@echo "doing nothing by default, try make periodic"
	@exit 1

periodic:
	rtgo build ./cmd/periodic
run-periodic: periodic Makefile
	sudo ./periodic
gdb-periodic: periodic Makefile
	sudo gdb ./periodic
dlv-periodic:
	sudo $(shell go env GOPATH)/bin/dlv debug ./cmd/periodic

.PHONY: periodic none run-periodic gdb-periodic
