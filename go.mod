module gitlab.com/rigel314/go-rt

go 1.13

require (
	github.com/containerd/cgroups v0.0.0-20200204152634-780d21166089
	github.com/opencontainers/runtime-spec v0.1.2-0.20190507144316-5b71a03e2700
	gitlab.com/rigel314/go-patch v0.0.2
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4
	gonum.org/v1/gonum v0.6.2
)

// replace gitlab.com/rigel314/go-patch => ../go-patch
