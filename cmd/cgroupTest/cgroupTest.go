/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// cgroupTest.go: Test code for learing to use the cgroups package

package main

import (
	"log"
	"time"

	"github.com/containerd/cgroups"
	"github.com/opencontainers/runtime-spec/specs-go"
	"golang.org/x/sys/unix"
)

func main() {
	cg, err := cgroups.New(cgroups.V1, cgroups.StaticPath("/go-rt_cpu0test"), &specs.LinuxResources{
		CPU: &specs.LinuxCPU{
			Cpus: "0",
		},
	})
	if err != nil {
		log.Fatal("cgroups.New", err)
	}
	if err := cg.Add(cgroups.Process{Pid: unix.Getpid()}); err != nil {
		log.Fatal("cg.Add", err)
	}

	log.Println(unix.Getpid())

	for {
		time.Sleep(time.Second)
	}
}
