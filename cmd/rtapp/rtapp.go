/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// rtapp.go: Very simple realtime app with one sched_deadline thread

package main

import (
	"log"
	"math"
	"runtime"
	"time"

	"gitlab.com/rigel314/go-rt"
	"gonum.org/v1/gonum/fourier"
)

const len = 10000

type printout struct {
	Timediff time.Duration
	Data     []float64
}

func init() {
	// Lock main thread here to prevent rt package from being able to lock the main thread
	runtime.LockOSThread()
}

func main() {
	ch := make(chan struct{}, 1)
	fftch := make(chan printout, 1)

	fft := fourier.NewFFT(len)
	src := make([]float64, len)
	dst := make([]complex128, len/2+1)
	fin := make([]float64, len)
	fin2 := make([]float64, len)
	// generate some data
	for i := 0; i < len; i++ {
		src[i] = math.Sin(2*math.Pi*10*(float64(i)/1000)) + math.Sin(2*math.Pi*2*(float64(i)/1000))
	}

	go func() {
		defer func() {
			ch <- struct{}{}
		}()

		th, err := rt.LockOSThread(rt.Task{
			Name:     "rt0",
			CPU:      []int{0},
			Period:   1000 * time.Millisecond,
			Deadline: 500 * time.Millisecond,
			Runtime:  250 * time.Millisecond,
		})
		if err != nil {
			log.Fatal("rt.LockOSThread error, ", err)
		}
		defer th.UnlockOSThread()

		log.Printf("%+v\n", th)

		waketime := time.Now()
		lastwaketime := time.Now()

		for i := 0; i < 10; i++ {
			fft.Coefficients(dst, src)
			f := &fin
			if i%2 == 1 {
				f = &fin2
			}
			fft.Sequence(*f, dst)
			fftch <- printout{Data: *f, Timediff: waketime.Sub(lastwaketime)}

			lastwaketime = waketime
			err := th.Yield()
			waketime = time.Now()
			if err != nil {
				log.Fatal("Yield error, ", err)
			}
		}
	}()

	go func() {
		for v := range fftch {
			log.Println(v.Timediff, v.Data[1:10])
		}
	}()

	<-ch
	log.Println("graceful exit")
}
