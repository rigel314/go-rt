/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// propagator.go: Control system for a more complicated realtime app

package main

import (
	"log"
	"time"

	"gitlab.com/rigel314/go-rt"
)

type propagator struct {
	r *refs
}

func (p *propagator) action(th rt.Thread) error {
	t1 := time.Now()
	for {
		p.r.propIn.Lock()
		p.r.propOut.Lock()

		t2 := time.Now()
		// log.Println("here")
		if t2.Sub(t1) > 150*time.Millisecond {
			log.Println(t2.Sub(t1))
		}
		t1 = t2

		p.r.propOut.Unlock()
		p.r.propIn.Unlock()
		th.Yield()
	}
}
