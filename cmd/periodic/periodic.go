/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// periodic.go: Entry point for a more complicated realtime app, with multiple sched_deadline threads, fake sensors, and a control system.

package main

import (
	"log"
	"runtime"
	"sync"
	"time"

	"gitlab.com/rigel314/go-rt"
)

func init() {
	// Lock main thread here to prevent rt package from being able to lock the main thread
	runtime.LockOSThread()
	// debug.SetGCPercent(-1)
	runtime.GOMAXPROCS(100)
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	ch := make(chan struct{}, 1)

	r := initRefs()

	rt.EnableTaskMap()

	wg := new(sync.WaitGroup)

	var rtsch []chan rt.Thread
	rtsch = append(rtsch, timed(0, wg, "propagator", 8, 10, &propagator{r: r}))
	rtsch = append(rtsch, timed(0, wg, "sense1", 4, 15, &sense{r: r, idx: 0}))
	rtsch = append(rtsch, timed(1, wg, "sense2", 9, 25, &sense{r: r, idx: 1}))

	wg.Wait()
	log.Print(rt.TaskTiming().EncodeToString())

	rts := make([]rt.Thread, len(rtsch))
	for i, v := range rtsch {
		rts[i] = <-v
	}

	go func() {
		for {
			time.Sleep(10 * time.Millisecond)
			// log.Println(runtime.NumGoroutine())
			for _, v := range rts {
				ts := v.Stats()
				if ts.MeasuredPeriod.Last > time.Millisecond*150 {
					log.Printf("%s: Period: %v, Runtime %v\n", v.Name(), ts.MeasuredPeriod.Last, ts.RunTime.Last)
				}
			}
			// fmt.Println()
		}
	}()

	select {}
}

const periodMs = 100

func timed(cpu int, wg *sync.WaitGroup, name string, worstRuntimeMs, deadlineMs int, action periodicAction) chan rt.Thread {
	ch := make(chan rt.Thread, 1)
	wg.Add(1)
	go func() {
		th, err := rt.LockOSThread(rt.Task{
			Name: name,
			CPU:  []int{cpu},
			// CPU:      []int{0, 1, 2, 3, 4, 5, 6, 7},
			Period:   100 * time.Millisecond,
			Deadline: time.Duration(deadlineMs) * time.Millisecond,
			Runtime:  time.Duration(worstRuntimeMs) * time.Millisecond,
		})
		if err != nil {
			log.Println("rt.LockOSThread error, ", err)
			wg.Done()
			return
		}

		th.EnableStats(true)
		ch <- th
		wg.Done()

		err = action.action(th)
		if err != nil {
			log.Println("action error, ", err)
			return
		}
	}()

	return ch
}
