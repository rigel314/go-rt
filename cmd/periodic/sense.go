/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// sense.go: Fake sensor to test a more complicated realtime app

package main

import "gitlab.com/rigel314/go-rt"

type sense struct {
	r   *refs
	idx int
}

func (s *sense) action(th rt.Thread) error {
	for {
		s.r.propIn.RLock()

		s.r.propIn.RUnlock()
		th.Yield()
	}
}
