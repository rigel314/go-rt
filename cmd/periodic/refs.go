package main

import (
	"sync"

	"gitlab.com/rigel314/go-rt"
)

type periodicAction interface {
	action(th rt.Thread) error
}

type lockableFloat64Slice struct {
	sync.RWMutex
	data []float64
}

type refs struct {
	propIn  *lockableFloat64Slice
	propOut *lockableFloat64Slice
}

func initRefs() *refs {
	return &refs{
		propIn:  &lockableFloat64Slice{data: make([]float64, 0, 100)},
		propOut: &lockableFloat64Slice{data: make([]float64, 0, 100)},
	}
}
