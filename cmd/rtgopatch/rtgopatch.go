/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// rtgopatch.go: Command to fetch, exctract, and patch a version of go to support sched_deadline

package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/rigel314/go-patch"
)

var destPath = flag.String("out", "", "Required.  Specifies output path for toolchain.")
var noFetch = flag.Bool("nofetch", false, "Pass to skip the fetch operation, and just do the patch.")
var noPatch = flag.Bool("nopatch", false, "Pass to skip the patch operation, and just do the fetch.")

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	if *destPath == "" {
		log.Fatal("Must provide -out option")
	}

	err := os.MkdirAll(*destPath, 0755)
	if err != nil {
		log.Fatal("Failed to create directory (", *destPath, "): ", err)
	}

	if !*noFetch {
		ents, err := ioutil.ReadDir(*destPath)
		if err != nil {
			log.Fatal("Failed to list directory (", *destPath, "): ", err)
		}
		if len(ents) > 0 {
			log.Println("Found entries:")
			for _, v := range ents {
				log.Println("\t", v.Name())
			}
			log.Fatal("Directory not empty (", *destPath, "): Giving up...")
		}

		c := new(http.Client)
		extension := ".tar.gz"
		switch runtime.GOOS {
		case "windows":
			extension = ".zip"
		}
		url := fmt.Sprintf("https://dl.google.com/go/go1.13.7.%s-%s%s", runtime.GOOS, runtime.GOARCH, extension)
		log.Println("Fetching/Extracting", url)
		resp, err := c.Get(url)
		if err != nil {
			log.Fatal("Get failed ("+url+"): ", err)
		}
		defer resp.Body.Close()

		// log.Printf("%+v\n", resp)

		// extract files from stream with zip or gzipped tar based on extension
		switch extension {
		case ".tar.gz":
			gzrd, err := gzip.NewReader(resp.Body)
			if err != nil {
				log.Fatal("gzip NewReader failed: ", err)
			}
			defer gzrd.Close()

			ard := tar.NewReader(gzrd)
			for {
				h, err := ard.Next()
				if err != nil {
					if err == io.EOF {
						break
					}
					log.Fatal("Next failed: ", err)
				}
				dst := filepath.Join(*destPath, h.Name)
				switch h.Typeflag {
				case tar.TypeReg:
					f, err := os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, h.FileInfo().Mode())
					if err != nil {
						log.Fatal("error creating file (", dst, "): ", err)
					}
					_, err = io.Copy(f, ard)
					if err != nil {
						log.Fatal("error writing file (", dst, "): ", err)
					}
					f.Close()
				case tar.TypeDir:
					err := os.MkdirAll(dst, 0755)
					if err != nil {
						log.Fatal("error creating directory (", dst, "): ", err)
					}
				default:
					log.Printf("Got unhandled type: %d\n", h.Typeflag)
				}
			}
		case ".zip":
			buf, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatal("readall failed: ", err)
			}
			brd := bytes.NewReader(buf)
			zrd, err := zip.NewReader(brd, int64(len(buf)))
			if err != nil {
				log.Fatal("zip.NewReader failed: ", err)
			}
			for _, zf := range zrd.File {
				zfrd, err := zf.Open()
				if err != nil {
					log.Fatal("error reading zip: ", err)
				}
				dst := filepath.Join(*destPath, zf.Name)
				if zf.Mode().IsRegular() {
					err = os.MkdirAll(filepath.Dir(dst), 0755)
					if err != nil {
						log.Fatal("error creating directory (", dst, "): ", err)
					}
					f, err := os.Create(dst)
					if err != nil {
						log.Fatal("error creating file (", dst, "): ", err)
					}
					_, err = io.Copy(f, zfrd)
					if err != nil {
						log.Fatal("error writing file (", dst, "): ", err)
					}
					f.Close()
					continue
				}
				if zf.Mode().IsDir() {
					err = os.MkdirAll(filepath.Dir(dst), 0755)
					if err != nil {
						log.Fatal("error creating directory (", dst, "): ", err)
					}
					continue
				}
				log.Printf("Strange mode (0x%x) on file (%s)\n", zf.Mode(), dst)
			}
		}
	}

	if !*noPatch {
		err = patch.Patch(*destPath, strings.NewReader(diff), 2, false)
		if err != nil {
			log.Fatal("Patch failed: ", err)
		}
	}
}

var diff = `
diff -ru /tmp/blar/go/src/runtime/lock_futex.go /tmp/blar2/go/src/runtime/lock_futex.go
--- /tmp/blar/go/src/runtime/lock_futex.go	2020-03-19 19:06:28.061942572 -0700
+++ /tmp/blar2/go/src/runtime/lock_futex.go	2020-03-19 19:38:03.267893308 -0700
@@ -29,7 +29,7 @@
 
 	active_spin     = 4
 	active_spin_cnt = 30
-	passive_spin    = 1
+	passive_spin    = 0
 )
 
 // Possible lock states are mutex_unlocked, mutex_locked and mutex_sleeping.
diff -ru /tmp/blar/go/src/runtime/lock_sema.go /tmp/blar2/go/src/runtime/lock_sema.go
--- /tmp/blar/go/src/runtime/lock_sema.go	2020-03-19 19:06:28.061942572 -0700
+++ /tmp/blar2/go/src/runtime/lock_sema.go	2020-03-19 19:38:38.084892403 -0700
@@ -29,7 +29,7 @@
 
 	active_spin     = 4
 	active_spin_cnt = 30
-	passive_spin    = 1
+	passive_spin    = 0
 )
 
 func lock(l *mutex) {
`
